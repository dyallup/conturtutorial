// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/SmearedJets.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MC_TUTORIAL : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MC_TUTORIAL);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections
      const FinalState fs;
      declare(FinalState(Cuts::abseta < 5 && Cuts::pT > 100*MeV), "fs");

      
      
      FastJets fj04(fs,  FastJets::ANTIKT, 0.4, JetAlg::NO_MUONS, JetAlg::NO_INVISIBLES);
      //fj04.useInvisibles();
      declare(fj04, "AntiKT04");

      declare(SmearedJets(fj04, JET_SMEAR_ATLAS_RUN2), "SmearedJets");

      
      // Book histograms
      _h1["jet1pt"] = bookHisto1D("jet1pt", 20, 0.0, 200.0);
      _h1["jet2pt"] = bookHisto1D("jet2pt", 20, 0.0, 200.0);
      _h1["mjj"] = bookHisto1D("mjj", 20, 0.0, 500.0);

      _h1["smeared_j1pt"] = bookHisto1D("smeared_j1pt", 20, 0.0, 200.0);


    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const double weight = event.weight();
      
      /// @todo Do the event by event analysis here
      Jets jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 25*GeV && Cuts::absrap < 4.4);

      Jets smearedJets = apply<JetAlg>(event, "SmearedJets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 4.4);
      
      double jpt1 = 0.0;
      double jpt2 = 0.0;
      double mjj = 0.0;

      double smearedjpt1 = 0.0;

      
      if (jets.size()){
	jpt1=jets[0].pT()/GeV;
      }

      if (smearedJets.size()){
	smearedjpt1 = smearedJets[0].pT()/GeV;
      }
      
      if (jets.size()>1){
	jpt2=jets[1].pT()/GeV;
	mjj = (jets[0].momentum() + jets[1].momentum()).mass()/GeV;
      }
    

      _h1["jet1pt"]->fill(jpt1,weight);
      _h1["jet2pt"]->fill(jpt2,weight);
      _h1["mjj"]->fill(mjj,weight);

      _h1["smeared_j1pt"]->fill(smearedjpt1,weight);
	
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double sf(crossSection() / sumOfWeights());
      for (HistoMap1D::value_type& hist : _h1) { scale(hist.second, sf); }

      

    }

    //@}

  private:
    typedef map<string, Histo1DPtr> HistoMap1D;
    HistoMap1D _h1;

  };

  

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_TUTORIAL);


}
